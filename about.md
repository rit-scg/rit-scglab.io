---
layout: page
title: About
permalink: "/about/"
tags: about
---

**When**: {{ site.data.meetings["time"] }}

**Where**: {{ site.data.meetings["location"] }}

The RIT Scientific Computing Group (RIT-SCG for short) is an informal group that seeks to create dialog between people that use computing in various scientific disciplines.  We aim to meet one day per week, and typical meetings consist of either a speaker showcasing their research, or a tutorial/workshop on a subject of interest to the group.  This group is not just for experts, we encourage people of all experience levels to attend, the only requirement is an interest to learn.

Speakers are chosen on a volunteer basis, so if you'd like to give a presentation, please [make a submission]({{ site.baseurl }}/submit/).  Keep in mind when preparing a talk that your audience will likely include people from very different backgrounds and experience levels, so try to keep the content accessible to non-experts.  Meetings last 1 hour, and unless the speaker objects, questions and constructive comments are encouraged throughout.

We are still getting a proper mailing list set up, so in the meantime please [contact us]({{ "/contact" | prepend: site.baseurl }}) and ask to be added to the informal mailing list.

Want to advertise our meetings in your department?  [Download our flyer]({{ site.baseurl }}/posters/scicomp-fall-2019-poster.pdf)
