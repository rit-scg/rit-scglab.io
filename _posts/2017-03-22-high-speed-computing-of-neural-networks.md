---
layout:     post
presenter:  "Nicholas Wilkins"
title:      "High Speed Computing of Neural Networks in mammalian brain vs. avian brain"
date:       2017-03-22 13:00:00
location:   "3225 Liberal Arts Hall"
categories: talks
---
