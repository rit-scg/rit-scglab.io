---
layout:     post
presenter:  "Ernest Fokoue"
title:      "On the Central Role of the Bayesian Paradigm in Statistical Machine Learning and Data Science"
date:       2017-11-27 13:25:00
location:   "A615 Slaughter Hall"
categories: talk
---

In this talk, I will provide a basic introduction to the Bayesian paradigm along with various examples illustrating its central role in statistical machine learning and data science. I will certainly highlight how the Bayesian school of thought gave rise to a multitude of fascinating and compelling scientific computing ideas and developments. I intend to make this talk informal and will eagerly welcome questions and remarks throughout my delivery of it.
