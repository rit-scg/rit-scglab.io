---
layout:     post
presenter:  "D. Joe Anderson"
title:      "Open Source Software in the Sciences"
date:       2018-04-23 14:50:00
location:   "4020 Frank E. Gannett Hall"
categories: workshop
---

RIT's Joe Anderson is going to talk about open source software, and its use in the sciences. He will bust some prominent myths and misunderstandings about open source, and dig into a few key fundamentals about free and open source software that align with some key goals in scientific work.
