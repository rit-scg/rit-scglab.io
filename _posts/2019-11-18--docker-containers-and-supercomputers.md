---
layout:     post
presenter:  "Justin Flory"
title:      "Docker containers + supercomputers = ??"
date:       2019-11-18 13:00:00
location:   "1350 Orange Hall"
categories: talk
---

How do you deliver research software into research computing
environments? Or supercomputers? Docker containers and related products
are revolutionizing IT infrastructure, but where do containers belong in
supercomputing / High-Performance Computing (HPC) infrastructure, on
large distributed computing grids? In a world of proprietary hardware
and drivers, large-scale distributed systems, and emphasis on bare-metal
performance, are containers another virtualization fad to skip over
supercomputing? Guess again.

This session explores different container run-times built for
supercomputing / HPC environments, what they offer, and some
benefits/costs of implementing them alongside HPC job scheduling
software. This session will be useful for you if you work with HPC
infrastructure, you write code intended to be run on parallel systems,
or you are interested for what the future of HPC technology might look like.

Read the blog post this discussion was centered around
[**here**](https://blog.justinwflory.com/2019/08/hpc-workloads-containers/}
