---
layout:     post
presenter:  "Greg Babbitt"
title:      "Taking the measure of mutation in the light of molecular dynamics"
date:       2018-03-05 14:30:00
location:   "4020 Frank E. Gannett Hall"
categories: talk
---

Dr. Gregory A. Babbitt (GAB) T.H. Gosnell School of Life Sciences, Rochester Institute of Technology.
Rochester, NY USA

Abstract – The proper statistical comparison of GPU accelerated molecular dynamic simulations on homologous proteins allows us to visualize the impact of one of the longest time scale processes in the universe, the molecular evolution over hundreds of millions of years, on one of the shortest time scales, the molecular mechanics occurring over nanoseconds. We have recently published user-friendly free software for comparative protein dynamics (DROIDS 1.2) that works as a backend to Amber16 while incorporating UCSF Chimera visualization to quantify statistically significant differences in the molecular motions of homologous PDB protein structures. DROIDS produces both quantitative plots and color-mapped dynamic structures that demonstrate both angstrom shifts in atom fluctuation on polypeptide backbone (dFLUX) and multiple test corrected p-values that highlight significant differences in dynamics due to both amino replacement and/or epigenetic modification. Here, we present a quantitative survey of the molecular dynamic impacts of mutation under different molecular evolutionary processes including functional conservation, adaptive evolution, gene duplication, and disease malfunction. We demonstrate that many congenital disease mutations significantly alter protein dynamics, in many cases towards a general global destabilization characterized by significantly increased dFLUX over the whole protein. Examples include well-known mutations responsible for cystic fibrosis, congenital cataract,
prion protein amyloidosis, Raf kinase melanoma, and Huntington’s disease. These disease-related mutations have much larger singular impacts on protein dynamics than most other mutations observed during normal molecular evolution. In contrast, the total dFLUX we observed when comparing functionally conserved orthologs and paralogs is exceedingly small, perhaps suggesting that some interesting biophysical limit(s) might constrain protein evolution (e.g. quantum tunneling). We will also discuss our current efforts to incorporate accelerated machine learning methods to detect and classify molecular dynamic signatures of mutations accumulating under various molecular evolutionary regimes.

Code repository at https://github.com/gbabbitt/DROIDS-1.0---free-software-project-for-comparative-protein-dynamics

View examples at https://www.youtube.com/channel/UCJTBqGq01pBCMDQikn566Kw
Relevant Publication
Babbitt, G.A., J.S. Mortensen, E.E. Coppola, L.E. Adams, and J.S. Liao. In press. DROIDS 1.2 – a GUI-based
pipeline for GPU-accelerated comparative protein dynamics. Biophysical Journal (CELL press).
