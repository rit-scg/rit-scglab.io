---
layout:     post
title:      "Semester Kickoff Meeting"
date:       2018-09-27 15:30:00
location:   "1154 Gosnell Hall"
categories: planning
---

First meeting of the Fall 2018 semester.  This will serve as an introduction for new members, including a quick recap of what the group has done in the past.  Then we will talk about what people would like to see done this semester, such as subjects they'd like to learn or speak about, and what we can do to make our meetings even better.
