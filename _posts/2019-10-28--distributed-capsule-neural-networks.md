---
layout:     post
presenter:  "Perry Deng"
title:      "Distributed Capsule Neural Networks – Applied Computer Vision Research"
date:       2019-10-28 13:00:00
location:   "1350 Orange Hall"
categories: talk
---

In this session, the presenter will briefly describe the use of distributed and
parallel GPU computing in his research experiments. He will also talk about the
researched topic, a new type of neural network inspired by data structures used
in computer vision, and Expectation-Maximization statistical algorithm.
