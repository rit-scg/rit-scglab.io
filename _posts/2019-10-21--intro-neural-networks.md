---
layout:     post
presenter:  "Marko Ristic"
title:      "Intro to Neural Networks with Applications to Function Approximation"
date:       2019-10-21 13:00:00
location:   "1350 Orange Hall"
categories: talk
---

Within the past decade, deep learning has become widely utilized in both
academic and industry settings. At the heart of deep learning lie neural
networks which combine linear algebra and statistics to mimic the learning
process which our brains undergo on a daily basis. The beginning of the talk
will focus on an elementary introduction to neural networks and their
functionalities. The latter portion of the talk will discuss how neural networks
can be used for function approximation in the context of parameter estimation.

[**Download slides here**]({{ site.baseurl }}/downloads/posts/2019-10-21--intro-neural-networks/Ristic_RITSCG_10-21-19.pdf)
