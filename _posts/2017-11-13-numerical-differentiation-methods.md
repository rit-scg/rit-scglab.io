---
layout:     post
presenter:  "Joshua Faber"
title:      "Numerical Differentiation Methods"
date:       2017-11-13 13:25:00
location:   "A615 Slaughter Hall"
categories: workshop
---

This will be an overview of various techniques for evaluating derivatives numerically, as well as rules of thumb and general advice for when to use each method. Audience questions are encouraged.
