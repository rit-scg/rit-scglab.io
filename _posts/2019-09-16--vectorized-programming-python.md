---
layout:     post
presenter:  "Daniel Wysocki"
title:      "Vectorized Programming in Python"
date:       2019-09-16 13:00:00
location:   "1350 Orange Hall"
categories: workshop
---

In this workshop, I will give an introduction to vectorized programming in Python.  While Python is not typically the most efficient language, with the right libraries installed you can speed things up immensely.  I will show you how to turn make your scientific Python code into highly efficient CPU code with the Numpy library, and how to translate that into efficient GPU code with the Cupy library.

Laptops are encouraged.  If you'd like to follow along, please come with the following installed:

- Numpy ([installation instructions](https://scipy.org/install.html))
- Cupy ([installation instructions](https://docs-cupy.chainer.org/en/stable/install.html#install-cupy))
  - Note: you will need an Nvidia GPU to run this

[**Download slides here**]({{ site.baseurl }}/downloads/posts/2019-09-16--vectorized-programming-python/wysocki_vectorized_python_slides.pdf)
