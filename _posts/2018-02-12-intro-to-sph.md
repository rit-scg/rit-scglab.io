---
layout:     post
presenter:  "Benjamin Lewis"
title:      "An Introduction to Smoothed Particle (Magneto-)Hydrodynamics"
date:       2018-02-12 14:30:00
location:   "4020 Frank E. Gannett Hall"
categories: talk
---

Smoothed particle hydrodynamics (SPH) is a mesh-free Lagrangian method for
solving fluid dynamics problems. SPH has been applied to a wide array of
problems, ranging from astrophysical and cosmological simulations to
modelling the flow of oil in an engine or waves breaking on a shore. In
this I will provide an introduction to SPH, including the underlying
mathematical basis. In addition I will cover various potential
implementation issues and known pitfalls.

Particularly in astrophysical calculations, the role
of magnetic fields can be important. Therefore I will then show how this
SF method can be extended to include magnetohydrodynamics (MHD),
including some of the unique difficulties caused by the nature of magnetic
fields.
