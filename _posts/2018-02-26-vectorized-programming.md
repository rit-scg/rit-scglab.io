---
layout:     post
presenter:  "Daniel Wysocki"
title:      "Tutorial on Vectorized Programming Languages"
date:       2018-02-26 14:30:00
location:   "4020 Frank E. Gannett Hall"
categories: workshop
---

Most of today's data analysis -- scientific or otherwise -- is performed in a vectorized programming language (e.g., Python+numpy, R, MATLAB). These languages are distinguished from more traditional "scalar" programming languages, in that one performs operations on entire arrays, instead of looping over each individual element.This talk will give a basic overview of the differences between scalar and vector languages, with side-by-side examples (in Python and C). I will explain both the convenience and performance motivations behind using such a language, as well as some downsides.If there is time at the end, I will show off more vectorization tricks in Python. Laptops with Python and IPython are recommended for this part.
