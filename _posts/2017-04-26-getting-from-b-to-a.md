---
layout:     post
presenter:  "Zachary Silberman"
title:      "Getting from B to A: The Inverse Curl Problem"
date:       2017-04-26 13:00:00
location:   "3225 Liberal Arts Hall"
categories: talks
---

Many different numerical codes are employed in studies of highly relativistic magnetized accretion flows around black holes. Based on the formalisms each uses, some codes evolve the magnetic field vector B, while others evolve the magnetic vector potential A, the two being related by the curl: B=curl(A). Here, we discuss how to generate vector potentials corresponding to specified magnetic fields on staggered grids, a surprisingly difficult task on finite cubic domains. The code we have developed solves this problem via a cell-by-cell method, whose scaling is nearly linear in the number of grid cells, and here we discuss the success this algorithm has in generating smooth vector potential configurations.
