---
layout:     post
presenter:  "Daniel Wysocki"
title:      "3D printing for scientific visualizations"
date:       2018-04-02 14:30:00
location:   "4020 Frank E. Gannett Hall"
categories: workshop
---

3D printing has recently become affordable and quite reliable. It is now being used to help visualize scientific data and mathematical functions -- basically anything that can be represented as $f(x, y)$ can trivially be printed.

This is going to be a very hands-on session: I will pass around some of my own 3D prints, and also show you how to make your own: from making the files to making affordable prints here at RIT. I will also be borrowing some resources from RIT's Dr Nate Barlow -- who had scheduling conflicts -- for printing solutions to differential equations.
