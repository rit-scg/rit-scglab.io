---
layout:     post
presenter:  "Benjamin Lewis"
title:      "Linux for new scientists: how to do useful things with your new laptop"
date:       2018-10-11 15:30:00
location:   "1154 Gosnell Hall"
categories: workshop
---

Unixes — especially Linux — are close to ubiquitous in scientific
computing. At least in astronomy! However, as an operating system Linux
is entirely unlike Windows and quite dissimilar to a Mac. In this talk I
will cover not so much how to use your new OS — that comes from time and
practice — but how to configure things, what all the jargon means, and
most importantly what to do with things go wrong.

And if I have time I'll show you some power-user secrets too :-)

**Note: Anybody with a Linux laptop is encouraged to bring it with them.**

[**Download slides here**]({{ site.baseurl }}/downloads/posts/2018-10-11-linux-for-new-scientists/Linux_Intro_Talk.pdf)
