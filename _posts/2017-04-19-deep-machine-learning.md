---
layout:     post
presenter:  "Christopher Kanan"
title:      "Deep Machine Learning: Overview and Applications"
date:       2017-04-19 13:00:00
location:   "3225 Liberal Arts Hall"
categories: talks
---

Over the past four years, deep machine learning has transformed artificial intelligence. With some caveats, these systems now surpass humans at some specialized tasks, such as identifying people in photos, classifying objects in images, and playing board games, such as Go. In this talk, I describe the technologies behind deep learning, along with the computer hardware necessary to run these algorithms. I discuss applications for these algorithms in which performance now rivals humans. Lastly, I describe ongoing work in my lab to apply deep learning algorithms to new problems and to fix its limitations.
