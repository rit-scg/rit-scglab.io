---
layout:     post
title:      "Semester Kickoff Meeting"
date:       2019-09-09 13:00:00
location:   "1350 Orange Hall"
categories: planning
---

First meeting of the Fall 2019 semester.  This will serve as an introduction for new members, including a quick recap of what the group has done in the past.  Then we will talk about what people would like to see done this semester, such as subjects they'd like to learn or speak about, and what we can do to make our meetings even better.
