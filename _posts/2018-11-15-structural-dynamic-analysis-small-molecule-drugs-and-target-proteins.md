---
layout:     post
presenter:  "Gregory Babbitt"
title:      "Structural dynamic analysis of small molecule drugs and their target proteins"
date:       2018-11-15 15:30:00
location:   "1154 Gosnell Hall"
categories: workshop
---

For this session we will use the KEGG relational database and UCSF Chimera to examine how drugs interact with target proteins in the signaling pathways of cells.  Bring a laptop with UCSF Chimera installed [[download here]](https://www.cgl.ucsf.edu/chimera/download.html) and I will demonstrate how to interact with the Protein Data Bank to find structures, analyze electron density and electrostatic surface charge, and run homology modeling, molecular docking and dynamic simulations.

The software is free, fun, friendly, fancy and likely will favorably faze your friends.
