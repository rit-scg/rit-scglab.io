---
layout:     post
presenter:  "George Thurston"
title:      "Computational and theoretical challenges for modeling screened electrostatic, charge-regulated interactions of proteins"
date:       2018-12-06 16:00:00
location:   "1154 Gosnell Hall"
categories: talk
---
