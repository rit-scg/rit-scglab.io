---
layout:     post
presenter:  "Joshua Faber"
title:      "Numerical Hydrodynamics"
date:       2018-11-01 15:30:00
location:   "1154 Gosnell Hall"
categories: talk
---

In this talk, I'll discuss how to implement a simple numerical hydrodynamics code, and discuss various pitfalls one can encounter along the way.  I'll discuss both of the leading options, grid-based Eulerian hydrodynamics and particle-based Lagrangian dynamics, noting which kinds of approach work better for which problems.  Time permitting, pretty pictures and movies will be shown.

[**Download slides**]({{ site.baseurl }}/downloads/posts/2018-11-01-numerical-hydrodynamics/josh-faber-computational-hydrodynamics.pdf)
