---
layout:     post
presenter:  "Jeffery Russell"
title:      "Programming in R"
date:       2019-09-30 13:00:00
location:   "1350 Orange Hall"
categories: workshop
---

Swing by this week's meeting to learn about the scientific programming language R. We will cover the basics of the language and dive into a fun interactive workshop.

If you'd like to follow along during the workshop, please bring a laptop with R installed.  You can download R from any of the [locations listed here](https://cran.r-project.org/mirrors.html)—it's recommended that you download from a geographically-nearby location for speed.

[**Slides available here**](https://jrtechs.net/blogContent/posts/data-science/html/IntroToR.html#/)
