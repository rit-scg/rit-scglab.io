---
layout:     post
presenter:  "Daniel Wysocki"
title:      "GPU Programming in Python"
date:       2018-10-18 15:30:00
location:   "1154 Gosnell Hall"
categories: workshop
---

I will give an overview of `CuPy`, an open-source Python library that serves as a high-level interface to the proprietary NVIDIA `CUDA` language.  It makes writing lightning-fast GPU accelerated code almost as easy as using Python's standard `NumPy` library, as it re-implements much of `NumPy`'s API.  Due to the ubiquitous nature of `NumPy` in scientific Python applications, `CuPy` will likely make porting your existing CPU code to a GPU as painless as possible.

This tutorial will include demos of essential features of `CuPy`, along with some general overview of GPUs, and some of the standard profiling tools you can use to measure your speed improvements and find the lines of code that are slowing you down.

People are encouraged to bring their laptops if they have access (possibly remotely) to a machine with a modern NVIDIA GPU, along with `CUDA` and `CuPy` installed.  It looks like the RIT research clusters (`rc.rit.edu`) only have older versions of `CUDA` (`CuPy` works in `CUDA` 8 and 9, but they have 7 and below), so this may be hit or miss.

[**Download slides**]({{ site.baseurl }}/downloads/posts//2018-10-18-gpu-programming-in-python/2018-10-18--gpu-programming-python-cuda-plain.pdf)

[**Download slides with speaker notes**]({{ site.baseurl }}/downloads/posts//2018-10-18-gpu-programming-in-python/2018-10-18--gpu-programming-python-cuda-notes.pdf)
