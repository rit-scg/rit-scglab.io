---
layout:     post
presenter:  "Mara Pudane"
title:      "Believable human group simulation"
date:       2017-11-06 13:25:00
location:   "A615 Slaughter Hall"
categories: talk
---

The human being is comprised of two parts: rational and emotional side. The emotional state impacts not only reasoning and behaviour of person who is experiencing it but also other people surrounding him.  In the seminar, computer modelling of human emotions, as well as human group emotions, will be discussed. Further, the listeners will be introduced to some exsting and currently in-progress applications of believable human simulation.
