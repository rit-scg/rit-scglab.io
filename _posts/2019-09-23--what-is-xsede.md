---
layout:     post
presenter:  "Joshua Faber"
title:      "What is XSede... and you can too!"
date:       2019-09-23 13:00:00
location:   "1350 Orange Hall"
categories: talk
---

Designing, building, running, and maintaining supercomputers can be a daunting task.  Thankfully, the US has XSede, an NSF-funded array of supercomputers, staff, and support that makes supercomputing available to all academic researchers in the country.  In this talk I'll tell you about XSede and the resources it provides, along with who may apply (probably not you, but definitely some people you know!), how much time you can get (quite a bit!) and how much work it takes to get started (1 page!  Really, just 1 page!).

[**Download slides here**]({{ site.baseurl }}/downloads/posts/2019-09-23--what-is-xsede/faber_xsede_slides.pdf)

[**Download example startup proposal here**]({{ site.baseurl }}/downloads/posts/2019-09-23--what-is-xsede/faber_xsede_startup_proposal.pdf)
