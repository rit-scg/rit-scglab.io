---
layout:     post
title:      "Semester Kickoff Meeting"
date:       2018-02-05 14:30:00
location:   "4020 Frank E. Gannett Hall"
categories: planning
---

First meeting of the semester. Introduction for new members, and planning for what people would like to see us do in the coming weeks and months.
