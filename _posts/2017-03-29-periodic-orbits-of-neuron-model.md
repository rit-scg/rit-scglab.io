---
layout:     post
presenter:  "Michael Radin"
title:      "Periodic orbits of a neuron model with a periodic internal decay rate"
date:       2017-03-29 13:00:00
location:   "3225 Liberal Arts Hall"
categories: talks
---

It is our goal to study the existence of periodic solutions, existence of eventually periodic solutions and the study of boundedness nature of solutions depending on the relationship between the periodic terms of the sequence of our neuron model; in particular, it is our objective to study which particular periodic cycles of various periods will exist, the patterns of these periodic cycles and their stability character as well. Furthermore, we will investigate which particular periodic cycles of our neuron model can be eventually periodic and why only particular ones be eventually periodic; we will analyze this behavior together with a bifurcation diagram as well. Moreover, we study different patterns of the transient terms of eventually periodic solutions to understand the phenomena more precisely.​
