---
layout:     post
presenter:  "Manuela Campanelli"
title:      "Light from the Dark: How Supercomputer Simulations of Binary Black Holes and Neutron Stars Can Reveal the Hidden Universe"
date:       2019-12-02 13:00:00
location:   "1350 Orange Hall"
categories: talk
---

The recent discoveries of gravitational waves from several binary black hole mergers, including the 2017 Nobel Prize-winning discovery, and a neutron star merger by the advanced LIGO and Virgo detectors are giving us the first glimpses of the hidden side of the universe. The first images of a supermassive black hole at the core of a galaxy have further demonstrated our incredible ability to capture the dimmest light from the invisible. In the next decade, the detection of low frequency gravitational waves by the Pulsar timing arrays and Laser Interferometer Space Antenna projects will unveil the mystery of merging supermassive black holes at the center of galaxies.

These amazing achievements require many telescopes stationed across the globe, thousands of researchers, and vast amounts of data to analyze. New advances in the field of theoretical and computational astrophysics are needed complement these observational successes. I will show how the most thorough and complete effort in the field is needed to simulate these extreme binary compact object coalescences from prior to merger through to the formation of disks and/or collapse of the merged remnant, the production of jets, and launching of outflows. In particular, I will present here new sophisticated simulations done on the NCSA’s Blue Water supercomputer indicating that supermassive binary black hole sources might indeed be detectable in the electromagnetic spectrum in the not too distant future.

**Speaker bio**

Manuela Campanelli is a Professor of Mathematics and Astrophysics at the Rochester Institute of Technology, where she leads the Center for Computational Relativity and Gravitation and the Astrophysics and Space Physics Institute of Research Excellence. She is a fellow of the American Physical Society, and of the International Society on General Relativity and Gravitation, and a member of the LIGO Scientific Collaboration.

Dr. Campanelli is known for groundbreaking work on binary black hole simulations, including the discovery of black hole “super kicks” and studies of spin-driven orbital dynamics and magnetized matter around them.  Her most current, NSF-funded research focuses on supercomputer simulations of merging supermassive black holes, binary neutron stars and on the magneto-hydrodynamics studies of their accretion disk and jet dynamics, in connection with both gravitational-wave and electromagnetic observations.  She is also working to advance core cyber-infrastructure in order to enable new science for multi-messenger astrophysics more broadly. For her largest magneto-hydrodynamical simulations, she uses large NSF funded peta-scale supercomputer systems, such the NSCA’s Blue Waters and TACC’s Frontera systems.
