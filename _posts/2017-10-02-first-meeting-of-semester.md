---
layout:     post
title:      "First Meeting of Semester"
date:       2017-10-02 13:25:00
location:   "A615 Slaughter Hall"
categories: planning
---

This is the first meeting of the semester for our group.

This will be a short meeting, mainly to introduce any new members, and explain what we do. We'll also be discussing what worked well last semester, what can be improved going forward, and what new things people would like to see done.
