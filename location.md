---
layout: page
title: Location
permalink: "/location/"
tags: about
---

This semester, Fall 2019, we are meeting in Orange Hall, room 1350, located a few steps east of Gosnell Hall.  There are two entrances on the north side, and if you take the east-most one, the room is straight ahead.

[![Image of campus map]({{ site.baseurl }}/campus-map.png)](https://maps.rit.edu/?details=Orange+Hall)
