# RIT Scientific Computing Group

[![pipeline status](https://gitlab.com/dwysocki/rit-scg/badges/master/pipeline.svg)](https://gitlab.com/dwysocki/rit-scg/commits/master) [![coverage report](https://gitlab.com/dwysocki/rit-scg/badges/master/coverage.svg)](https://gitlab.com/dwysocki/rit-scg/commits/master)



This is the repository that holds the source code for the RIT Scientific Computing Group.
